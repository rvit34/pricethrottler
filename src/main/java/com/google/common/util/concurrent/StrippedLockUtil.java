package com.google.common.util.concurrent;

import java.util.concurrent.locks.Lock;

public class StrippedLockUtil {

     private StrippedLockUtil() { }

     @SuppressWarnings("UnstableApiUsage")
     public static Striped<Lock> fromLock(int size, Lock lock) { return Striped.custom(size, () -> lock); }
}
