package com.price.processor;

import com.price.processor.util.Statics;
import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.time.TimeProvider;
import net.openhft.chronicle.core.time.UniqueMicroTimeProvider;
import org.apache.log4j.BasicConfigurator;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static com.price.processor.util.Statics.SBP;

@Slf4j
public class PriceThrottler implements PriceProcessor, AutoCloseable {

    private final Map<PriceProcessor, ProcessingUnit> subscribers;

    private final Configuration configuration;
    private final TimeProvider timeProvider;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final ExecutorService fastWorkers = Executors.newCachedThreadPool();
    private final ExecutorService slowWorkers;

    static {
        BasicConfigurator.configure();
    }

    public PriceThrottler(Configuration configuration, TimeProvider timeProvider) {
        this.configuration = configuration;
        this.timeProvider = timeProvider;
        subscribers = new ConcurrentHashMap<>(configuration.getMaxSubscribers(), 1.0f);
        scheduler.scheduleWithFixedDelay(this::notifySubscribers, 0, configuration.getSubscriberNotificationRateInMillis(), TimeUnit.MILLISECONDS);
        slowWorkers = Executors.newWorkStealingPool(configuration.getMaxNativeThreadsForSlowMode());
        registerShutdown();
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        for (ProcessingUnit processingUnit : subscribers.values()) {
            processingUnit.onPrice(ccyPair, rate);
        }
    }

    public void subscribeAll(List<? extends PriceProcessor> priceProcessors) {
        for (PriceProcessor processor : priceProcessors) { subscribe(processor); }
    }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        final ProcessingUnit processingUnit = subscribers.computeIfAbsent(priceProcessor, processor -> {
            if (subscribers.size() >= configuration.getMaxSubscribers()) {
                return null;
            }
            return ProcessingUnit.getInstance(ProcessingUnit.Impls.LAST_PRICE, configuration, timeProvider, priceProcessor);
        });
        if (processingUnit == null) {
            StringBuilder errorMsg = SBP.acquireStringBuilder();
            errorMsg.append("You exceeded a limit of price change subscribers.");
            errorMsg.append("Max:");
            errorMsg.append(configuration.getMaxSubscribers());
            throw new LimitExceededException(errorMsg.toString());
        }
    }

    public void unsubscribeAll(List<? extends PriceProcessor> priceProcessors) {
        for (PriceProcessor processor : priceProcessors) { unsubscribe(processor); }
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) { subscribers.remove(priceProcessor); }

    @Override
    public void close() { gracefullyShutdown(); }

    public void closeImmediately() {
        scheduler.shutdownNow();
        fastWorkers.shutdownNow();
        slowWorkers.shutdownNow();
    }

    private void registerShutdown() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::gracefullyShutdown));
    }

    private void gracefullyShutdown() {
        log.info("shutting down executors gracefully...");
        Statics.gracefullyShutdown(scheduler, 5);
        Statics.gracefullyShutdown(fastWorkers, 15);
        Statics.gracefullyShutdown(slowWorkers, 30);
        log.info("All executors have been shut down");
    }

    private void notifySubscribers() {
        for (ProcessingUnit processingUnit : subscribers.values()) {
            processingUnit.processFastUpdates(fastWorkers);
        }
        for (ProcessingUnit processingUnit : subscribers.values()) {
            processingUnit.processSlowUpdates(slowWorkers);
        }
    }

    public static void main(String[] args) {
        new PriceThrottler(Configuration.defaultConfiguration(), new UniqueMicroTimeProvider());
    }
}
