package com.price.processor.util;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class AtomicLock implements Lock {

    private final AtomicBoolean monitor = new AtomicBoolean(false);
    private ThreadLocal<Long> ownerThread;

    private final boolean trackOwnerThread;

    public AtomicLock() { this(true); }

    public AtomicLock(boolean trackOwnerThread) {
        this.trackOwnerThread = trackOwnerThread;
        if (trackOwnerThread) {
            ThreadLocal.withInitial(() -> 0L);
        }
    }

    @Override
    public void lock() { while (!tryLock()) {} }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        while (!tryLock()) {
            if (Thread.interrupted()) throw new InterruptedException();
        }
    }

    @Override
    public boolean tryLock() {
        final boolean blocked = monitor.get();
        final boolean acquired = !blocked && monitor.compareAndSet(false, true);
        if (trackOwnerThread && acquired) {
            ownerThread.set(Thread.currentThread().getId());
        }
        return acquired;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        long start = unit.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
        final long end = start + time;
        long current;
        while (!tryLock()) {
            if (Thread.interrupted()) throw new InterruptedException();
            current = unit.convert(System.nanoTime(), TimeUnit.NANOSECONDS);
            if (current > end) return false;
        }
        return true;
    }

    @Override
    public void unlock() {
        if (trackOwnerThread) {
            Long ownerId = ownerThread.get();
            if (ownerId.equals(Thread.currentThread().getId())) {
                monitor.set(false);
            }
        } else {
            monitor.set(false);
        }
    }

    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException("not implemented yet");
    }
}
