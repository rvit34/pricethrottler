package com.price.processor.util;

import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.pool.StringBuilderPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.function.DoubleSupplier;
import java.util.function.LongSupplier;

@Slf4j
public class Statics {

    private Statics() { } // static library should not be instantiated

    public static final StringBuilderPool SBP = new StringBuilderPool();

    public static void withLock(Lock lock, Runnable code) {
        try {
            lock.lock();
            code.run();
        } finally {
            lock.unlock();
        }
    }

    public static long withLock(Lock lock, LongSupplier longSupplier) {
        try {
            lock.lock();
            return longSupplier.getAsLong();
        } finally {
            lock.unlock();
        }
    }

    public static double withLock(Lock lock, DoubleSupplier doubleSupplier) {
        try {
            lock.lock();
            return doubleSupplier.getAsDouble();
        } finally {
            lock.unlock();
        }
    }

    public static void gracefullyShutdown(ExecutorService executorService, int waitTimeInSeconds) {
        if (!executorService.isShutdown()) {
            executorService.shutdown();
            try {
                if (!executorService.awaitTermination(waitTimeInSeconds, TimeUnit.SECONDS)) {
                    log.warn("Thread pool {} did not terminate in the specified time.", executorService);
                    executorService.shutdownNow();
                }
            } catch (InterruptedException e) {
                log.warn("Thread pool {} could not terminate properly.", executorService);
                executorService.shutdownNow();
            }
        }
    }
}
