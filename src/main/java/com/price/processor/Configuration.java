package com.price.processor;

import lombok.Builder;
import lombok.Getter;
import javax.annotation.concurrent.Immutable;

@Builder
@Getter
@Immutable
public final class Configuration {

    // general
    private final int maxSubscribers;
    private final int maxCcyPairs;

    // throttling (only for stack-based implementation)
    private final int maxStackDepth;
    private final int minFrequencyInMillis;

    // scheduler
    /**
     *  how often {@link PriceThrottler} runs notification procedure on all subscribers. This param might be considered as a high-level throttling.
    */
    private final int subscriberNotificationRateInMillis;

    /**
     * this param splits subscribers on fast and slow ones. If a subscriber executes price update on some pair within this threshold it treats as fast on this pair
     */
    private final int fastModeThresholdInMillis;

    /**
     * this param sets upper limit for native(os) threads from shared thread pool that might be utilized to process price updates on slow cases/modes
     * don't set this value too high (keep in mind that 1 native thread consumes ~1mb of native memory by default)
     * don't set this value too small (if you have N slow subscribers that use CPU intensively you should set count of native threads greater or equal to N)
     */
    private final int maxNativeThreadsForSlowMode;

    public static Configuration defaultConfiguration() {
        return Configuration.builder()
                .maxSubscribers(200)
                .maxCcyPairs(200)
                .maxStackDepth(1)
                .minFrequencyInMillis(1000)
                .subscriberNotificationRateInMillis(1000) // subscriber notifications will be called roughly once per second (such way we may skip too often updates)
                .fastModeThresholdInMillis(1000) // processing within 1sec is considered as fast
                .maxNativeThreadsForSlowMode(512)
                .build();
    }
}
