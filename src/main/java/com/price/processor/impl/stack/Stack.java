package com.price.processor.impl.stack;

import javax.annotation.Nullable;

interface Stack<E> {

    /**
     * set head of the stack to this element
     * @param element - a new top of the stack
     */
    void push(E element);

    /**
     * @return and remove head of the stack or null if stack is empty
     */
    @Nullable
    E pop();

    /**
     * @return head of the stack or null if stack is empty
     */
    @Nullable
    E head();
}
