package com.price.processor.impl.stack;

import lombok.Getter;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.TimeUnit;

@Immutable
@Getter
public final class PriceChangeUpdate implements TimedElement {

    private final String ccyPair;
    private final double price;
    private final long timestampInNanos;

    public PriceChangeUpdate(String ccy, double price, long timestampInNanos) {
        this.ccyPair = ccy;
        this.price = price;
        this.timestampInNanos = timestampInNanos;
    }

    @Override
    public final long timestamp(TimeUnit timeUnit) { return TimeUnit.NANOSECONDS.convert(timestampInNanos, timeUnit); }
}
