package com.price.processor.impl.stack;

import java.util.concurrent.TimeUnit;

interface TimedElement {
    long timestamp(TimeUnit timeUnit);
}
