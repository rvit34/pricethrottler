package com.price.processor.impl.stack;

import com.price.processor.util.AtomicLock;

import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * Throttling and thread-safe implementation of {@link Stack}.<br>
 * Non-blocking CAS synchronization is used (see {@link AtomicLock}) to make concurrent push+throttle and pop operations atomic.<br>
 * Throttling might utilize two strategies:<br>
 * Size-based: elements which deeper than maximum depth of stack are removed<br>
 * Frequency-based: too fresh elements also removed from the stack (timestamp difference between top and each checked element is used to determine frequency)<br>
 */
@ThreadSafe
class ThrottlingStack<E extends TimedElement> implements Stack<E> {

    @GuardedBy("lock")
    private final Deque<E> deque = new LinkedList<>();
    private final Lock lock = new AtomicLock(false);

    private final int maxDepth;
    private final long minFrequency;
    private final TimeUnit minFrequencyTimeUnit;

    public ThrottlingStack(int maxDepth) {
        this(maxDepth, -1, TimeUnit.MILLISECONDS);
    }

    public ThrottlingStack(long minFrequency, TimeUnit minFrequencyTimeUnit) {
        this(-1, -minFrequency, minFrequencyTimeUnit);
    }

    /**
     * Creates a new instance of ThrottlingStack <br>
     * @param maxDepth - maximum depth of the stack size. all elements from the top(exclusive) which will cause deeper stack are removed.<br>
     * if maxDepth is zero or negative then this strategy won't be used. <br>
     * @param minFrequency - minimum duration between top and current pushable element. <br>
     * All elements starting from maxDepth or top(exclusive) will be checked and removed if their durations is less than minFrequency<br>
     * if minFrequency is zero or negative then this strategy won't be used. <br>
     * @param minFrequencyTimeUnit - a type of {@link TimeUnit}
     */
    public ThrottlingStack(int maxDepth, long minFrequency, TimeUnit minFrequencyTimeUnit) {
        this.maxDepth = maxDepth;
        this.minFrequency = minFrequency;
        this.minFrequencyTimeUnit = minFrequencyTimeUnit;
    }

    @Override
    public void push(E newHead) {
       if (newHead == null) return;
       try {
           lock.lock();
           if (isNew(newHead)) {
               deque.push(newHead);
               throttleByMaxSize();
               throttleByMinFrequency();
           }
       } finally {
           lock.unlock();
       }
    }

    @Override
    @Nullable
    public E pop() {
      try {
          lock.lock();
          if (deque.isEmpty()) return null;
          return deque.pop();
      } finally {
          lock.unlock();
      }
    }

    @Nullable
    @Override
    public E head() {
        try {
            lock.lock();
            return deque.peekFirst();
        } finally {
            lock.unlock();
        }
    }

    private boolean isNew(E newHead) {
        final E head = deque.peekFirst();
        return head == null || newHead.timestamp(TimeUnit.NANOSECONDS) >= head.timestamp(TimeUnit.NANOSECONDS);
    }

    private void throttleByMaxSize() {
        if (maxDepth <= 0) return;
        int overloadedElementsCount = deque.size() - maxDepth;
        if (overloadedElementsCount > 0) {
            final Iterator<E> it = deque.descendingIterator();
            int countElementsToRemove = overloadedElementsCount;
            while (it.hasNext() && countElementsToRemove > 0) {
                it.remove();
                countElementsToRemove--;
            }
        }
    }

    private void throttleByMinFrequency() {
        if (minFrequency <= 0) return;
        int nonRemovableElementsCount = maxDepth <= 0 ? 1 : maxDepth;
        if (deque.size() <= nonRemovableElementsCount) return;
        int countElementsToRemove = deque.size() - nonRemovableElementsCount;
        E top = deque.getFirst();
        final Iterator<E> it = deque.descendingIterator();
        long frequency;
        while (it.hasNext() && countElementsToRemove > 0) {
            frequency = it.next().timestamp(minFrequencyTimeUnit) - top.timestamp(minFrequencyTimeUnit);
            if (frequency < minFrequency) {
                it.remove();
            }
            countElementsToRemove--;
        }
    }
}
