package com.price.processor.impl.stack;

import com.price.processor.*;
import com.price.processor.impl.common.Stats;
import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.time.TimeProvider;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static com.price.processor.util.Statics.*;

/**
* Implementation is based on {@link ThrottlingStack} <br>
* pros: ability to apply a few throttling strategies and control the size of stack. <br>
* cons: produces a lot of garbage, uses native threads <br>
*/
@Slf4j
public class StackBasedProcessingUnit implements ProcessingUnit {

    // key -> ccyPair, value -> stack of price updates
    private final Map<String, Stack<PriceChangeUpdate>> priceUpdates;

    private final Configuration configuration;
    private final TimeProvider timeProvider;
    private final PriceProcessor priceProcessor;
    private final Stats stats;

    public StackBasedProcessingUnit(
            Configuration configuration,
            TimeProvider timeProvider,
            PriceProcessor priceProcessor) {

        this.configuration = configuration;
        this.timeProvider = timeProvider;
        this.priceProcessor = priceProcessor;
        priceUpdates = new ConcurrentHashMap<>(configuration.getMaxCcyPairs(), 1.0f);
        stats = new Stats(configuration.getMaxCcyPairs());
    }

    /**
     * significant drawback: allocates a new object at heap memory on every price update -> frequent gc pauses -> very bad for low latency
     */
    @Override
    public void onPrice(String ccyPair, double rate) {
       Stack<PriceChangeUpdate> updatedStack = priceUpdates.compute(ccyPair, (pair, updates) -> {
            if (updates == null) {
                if (priceUpdates.size() >= configuration.getMaxCcyPairs()) {
                    return null;
                } else {
                    updates = new ThrottlingStack<>(
                            configuration.getMaxStackDepth(),
                            configuration.getMinFrequencyInMillis(),
                            TimeUnit.MILLISECONDS
                    );
                }
            }
            updates.push(new PriceChangeUpdate(ccyPair, rate, timeProvider.currentTimeNanos()));
            stats.initElapsedTime(ccyPair, false);
            stats.incrementIncomingRate(ccyPair, false);
            return updates;
        });
       if (updatedStack == null) {
           final StringBuilder errorMsg = SBP.acquireStringBuilder();
           errorMsg.append("number of currency pairs is greater than limit ");
           errorMsg.append(configuration.getMaxCcyPairs());
           throw new LimitExceededException(errorMsg.toString());
       }
    }

    @Override
    public void processFastUpdates(ExecutorService fastWorkers) {
        processUpdates(stats.pairsForFastProcessing(configuration.getFastModeThresholdInMillis()), fastWorkers);
    }

    @Override
    public void processSlowUpdates(ExecutorService slowWorkers) {
        processUpdates(stats.pairsForSlowProcessing(configuration.getFastModeThresholdInMillis()), slowWorkers);
    }

    /**
     * drawback: native threads are used to process price updates on all pairs, so we might exhaust shared thread pool quite fast
     */
    private void processUpdates(List<String> ccyPairs, ExecutorService executorService) {
        for (String ccyPair : ccyPairs) {
            CompletableFuture.runAsync(() -> processUpdate(ccyPair), executorService);
        }
    }

    private void processUpdate(String ccyPair) {
        Stack<PriceChangeUpdate> updates = priceUpdates.get(ccyPair);
        final PriceChangeUpdate lastPriceUpdate = updates.pop();
        if (lastPriceUpdate != null) {
            final long before = timeProvider.currentTimeNanos();
            try {
                log.debug("start processing update on {}", priceProcessor.toString());
                stats.markAsProcessing(ccyPair);
                priceProcessor.onPrice(ccyPair, lastPriceUpdate.getPrice());
                log.debug("price update was processed on {}", priceProcessor.toString());
            } catch (Exception ex) {
                log.error("price processor could not complete its execution", ex);
                updates.push(lastPriceUpdate);
            } finally {
                stats.markAsProcessed(ccyPair, timeProvider.currentTimeNanos() - before);
            }
        }
    }
}
