package com.price.processor.impl.lastprice;

import com.google.common.util.concurrent.Striped;
import com.google.common.util.concurrent.StrippedLockUtil;
import com.price.processor.Configuration;
import com.price.processor.PriceProcessor;
import com.price.processor.ProcessingUnit;
import com.price.processor.impl.common.Stats;
import com.price.processor.util.AtomicLock;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.time.TimeProvider;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;

import static com.price.processor.util.Statics.withLock;

/**
 * The unit stores in RAM only last price update for each currency pair. <br>
 * Next optimizations are applied: <br>
 * - <a href="https://fastutil.di.unimi.it/">primitive collections</a> are used to prevent autoboxing and random memory hits <br>
 * - collections are guarded by fine grained locking based on <a href="https://github.com/google/guava/wiki/StripedExplained">Guava Stripped Locks</a> and {@link AtomicLock} <br>
 * - each price update is executed in lightweight virtual thread. See <a href="https://wiki.openjdk.java.net/display/loom/Main">Project Loom</a>
 */
@ThreadSafe
@Slf4j
public class LastPriceProcessingUnit implements ProcessingUnit {

    private static final double PRICE_NOT_SET = -1D;

    @SuppressWarnings("UnstableApiUsage")
    private final Striped<Lock> locks;

    @GuardedBy("locks")
    // key - ccyPair, value -> last known rate
    private final Object2DoubleMap<String> priceUpdates;

    private final Stats stats;

    private final Configuration configuration;

    private final PriceProcessor priceProcessor;

    private final TimeProvider timeProvider;


    public LastPriceProcessingUnit(
            Configuration configuration,
            TimeProvider timeProvider,
            PriceProcessor priceProcessor) {

        this.configuration = configuration;
        this.priceProcessor = priceProcessor;
        this.timeProvider = timeProvider;
        locks = StrippedLockUtil.fromLock(configuration.getMaxCcyPairs(), new AtomicLock(false));
        priceUpdates = new Object2DoubleOpenHashMap<>(configuration.getMaxCcyPairs() + 1, 0.99f);
        priceUpdates.defaultReturnValue(PRICE_NOT_SET);
        stats = new Stats(configuration.getMaxCcyPairs());
        // todo: it would be better to pre-initialize all keys in prices collection here but we don't know all possible ccyPairs in advance
    }

    @Override
    public void onPrice(String ccyPair, double rate) { updatePrice(ccyPair, rate); }

    @Override
    public void processFastUpdates(ExecutorService fastWorkers) {
        final List<String> pairs = stats.pairsForFastProcessing(configuration.getFastModeThresholdInMillis());
        processUpdates(pairs, fastWorkers);
    }

    @Override
    public void processSlowUpdates(ExecutorService slowWorkers) {
        final List<String> pairs = stats.pairsForSlowProcessing(configuration.getFastModeThresholdInMillis());
        processUpdates(pairs, slowWorkers);
    }

    private void processUpdates(List<String> ccyPairs, ExecutorService executor) {
        for (String ccyPair : ccyPairs) {
            Thread.builder()
                    .virtual(executor)
                    .task(() -> processPairUpdate(ccyPair))
                    .build()
            .start();
        }
    }

    private void processPairUpdate(String ccyPair) {
        final double lastPrice = lastPrice(ccyPair);
        if (lastPrice == PRICE_NOT_SET) return;
        final long timeBefore = timeProvider.currentTimeNanos();
        final long incomingRate = stats.currentIncomingRate(ccyPair);
        try {
            log.debug("updating price[pair={},price={}] on processor {}", ccyPair, lastPrice, priceProcessor);
            stats.markAsProcessing(ccyPair);
            priceProcessor.onPrice(ccyPair, lastPrice);
            log.debug("processor {} got update", priceProcessor);
            stats.addToRate(ccyPair, -incomingRate, true);
        } catch (Exception ex) {
            log.error("price processor could not complete its execution", ex);
        } finally {
            markAsProcessed(ccyPair, lastPrice, timeProvider.currentTimeNanos() - timeBefore);
        }
    }

    private void updatePrice(String ccyPair, double rate) {
        withLock(locks.get(ccyPair), () -> {
            priceUpdates.put(ccyPair, rate);
            stats.incrementIncomingRate(ccyPair, false);
            stats.initElapsedTime(ccyPair, false);
        });
    }

    private void markAsProcessed(String ccyPair, double oldRate, long consumedTimeInNanos) {
        withLock(locks.get(ccyPair), () -> {
            priceUpdates.replace(ccyPair, oldRate, PRICE_NOT_SET);
            stats.markAsProcessed(ccyPair, consumedTimeInNanos);
        });
    }

    private double lastPrice(String ccyPair) {
        return withLock(locks.get(ccyPair), () -> priceUpdates.getOrDefault(ccyPair, PRICE_NOT_SET));
    }
}
