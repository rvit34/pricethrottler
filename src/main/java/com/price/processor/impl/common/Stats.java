package com.price.processor.impl.common;

import com.google.common.collect.ComparisonChain;
import com.google.common.util.concurrent.Striped;
import com.google.common.util.concurrent.StrippedLockUtil;
import com.price.processor.util.AtomicLock;
import it.unimi.dsi.fastutil.objects.Object2LongArrayMap;
import it.unimi.dsi.fastutil.objects.Object2LongMap;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.concurrent.GuardedBy;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

import static com.price.processor.util.Statics.withLock;

@Slf4j
public class Stats {

    private static final long DEFAULT_INCOMING_RATE = 0L;
    private static final long DEFAULT_ELAPSED_TIME = 0L;
    private static final long PAIR_IS_PROCESSING = -1L;

    @SuppressWarnings("UnstableApiUsage")
    private final Striped<Lock> locks;

    // key - ccyPair, value -> incoming rate. The count of messages since last time subscriber was notified
    @GuardedBy("locks")
    private final Object2LongMap<String> incomingRates;

    // key - ccyPair, value - last known elapsed time in nanos
    @GuardedBy("locks")
    private final Object2LongMap<String> elapsedTimes;

    // copies are not thread-safe
    private Object2LongMap<String> incomingRatesDefensiveCopy;
    private final Object2LongMap<String> elapsedTimeDefensiveCopy;

    public Stats(int maxCcyPairs) {
        locks = StrippedLockUtil.fromLock(maxCcyPairs, new AtomicLock(false));
        incomingRates = new Object2LongOpenHashMap<>(maxCcyPairs + 1, 0.99f);
        elapsedTimes = new Object2LongOpenHashMap<>(maxCcyPairs + 1, 0.99f);
        incomingRatesDefensiveCopy = new Object2LongOpenHashMap<>(maxCcyPairs + 1, 0.99f);
        elapsedTimeDefensiveCopy = new Object2LongArrayMap<>();
        incomingRates.defaultReturnValue(DEFAULT_INCOMING_RATE);
        elapsedTimes.defaultReturnValue(DEFAULT_ELAPSED_TIME);
    }


    /**
     * @return a list of ccy pairs those last known times taken for execute price update is less than or equal to threshold
     * Important note: don't call it from multiple threads with contention
     */
    public List<String> pairsForFastProcessing(final long thresholdInMillis) {
        elapsedTimeDefensiveCopy.clear();
        elapsedTimeDefensiveCopy.putAll(elapsedTimes);

        return elapsedTimeDefensiveCopy.object2LongEntrySet().stream()
                .filter(time -> time.getLongValue() > DEFAULT_ELAPSED_TIME && time.getLongValue() <= TimeUnit.MILLISECONDS.toNanos(thresholdInMillis))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     * @return a list of ccy pairs those last known times taken for execute price update is more than or equal to threshold.
     * We threat first execution as slow, so new pairs are also included to the list.
     * Additionally pairs sorted by last known elapsed time and incoming rate in asc order.
     * i.e. faster subscribers with rarely changing prices should go first
     * Important note: don't call it from multiple threads with contention
     */
    public List<String> pairsForSlowProcessing(final long thresholdInMillis) {
        incomingRatesDefensiveCopy.clear();
        elapsedTimeDefensiveCopy.clear();
        incomingRatesDefensiveCopy.putAll(incomingRates);
        elapsedTimeDefensiveCopy.putAll(elapsedTimes);

        return elapsedTimeDefensiveCopy.object2LongEntrySet().stream()
                .filter(time -> time.getLongValue() == DEFAULT_ELAPSED_TIME || time.getLongValue() > thresholdInMillis)
                // fast subscribers have more priority than slow ones
                // and
                // to do not miss updates, rarely changing prices should be processed first
                // so
                // we most interested in fast subscribers with rarely changing prices
                .sorted(this.pairsComparator)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private final Comparator<Object2LongMap.Entry<String>> pairsComparator = (p1, p2) -> ComparisonChain.start()
            .compare(p1.getLongValue(), p2.getLongValue()) // order by elapsed time
            .compare( // and order by incoming rates
                incomingRatesDefensiveCopy.getOrDefault(p1.getKey(), DEFAULT_INCOMING_RATE),
                incomingRatesDefensiveCopy.getOrDefault(p2.getKey(), DEFAULT_INCOMING_RATE)
            )
            .result();

    public long currentIncomingRate(String ccyPair) {
        return withLock(locks.get(ccyPair), () -> incomingRates.getOrDefault(ccyPair, DEFAULT_INCOMING_RATE));
    }

    public void incrementIncomingRate(String ccyPair, boolean sync) {
        addToRate(ccyPair, 1L, sync);
    }

    public void addToRate(String ccyPair, long delta, boolean sync) {
        if (sync) {
            withLock(locks.get(ccyPair), () -> incomingRates.put(ccyPair, incomingRates.getOrDefault(ccyPair, DEFAULT_INCOMING_RATE) + delta));
        } else {
            incomingRates.put(ccyPair, incomingRates.getOrDefault(ccyPair, DEFAULT_INCOMING_RATE) + delta);
        }
    }

    public void markAsProcessing(String ccyPair) {
        withLock(locks.get(ccyPair), () -> elapsedTimes.put(ccyPair, PAIR_IS_PROCESSING));
    }

    public void markAsProcessed(String ccyPair, long elapsedTimeInNanos) {
        withLock(locks.get(ccyPair), () -> elapsedTimes.put(ccyPair, elapsedTimeInNanos));
    }

    public void initElapsedTime(String ccyPair, boolean sync) {
        if (sync) {
            withLock(locks.get(ccyPair), () -> elapsedTimes.putIfAbsent(ccyPair, DEFAULT_ELAPSED_TIME));
        } else {
            elapsedTimes.putIfAbsent(ccyPair, DEFAULT_ELAPSED_TIME);
        }
    }
}
