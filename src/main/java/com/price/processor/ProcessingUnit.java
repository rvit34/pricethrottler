package com.price.processor;


import com.price.processor.impl.lastprice.LastPriceProcessingUnit;
import com.price.processor.impl.stack.StackBasedProcessingUnit;
import net.openhft.chronicle.core.time.TimeProvider;

import java.util.concurrent.ExecutorService;

/**
 * The implementations of this interface have to include some additional logic before and after the call on price processor.
 */
public interface ProcessingUnit {

    /**
     * called on each price update from upstream ({@link PriceThrottler})
     * @param ccyPair - EURUSD, EURRUB, USDJPY - up to 200 different currency pairs
     * @param rate - any double rate like 1.12, 200.23 etc
     */
    void onPrice(String ccyPair, double rate);

    /**
     * call it when you need to notify price processor on price updates by some pairs that were processed fast recently
     */
    void processFastUpdates(ExecutorService fastWorkers);

    /**
     * call it when you need to notify price processor on price updates by some pairs that were processed slow recently
     */
    void processSlowUpdates(ExecutorService slowWorkers);


    enum Impls {
        STACK_BASED, LAST_PRICE
    }

    static ProcessingUnit getInstance(Impls impl,
                                      Configuration configuration,
                                      TimeProvider timeProvider,
                                      PriceProcessor priceProcessor) {
        if (impl == Impls.STACK_BASED) {
            return new StackBasedProcessingUnit(configuration, timeProvider, priceProcessor);
        }
        return new LastPriceProcessingUnit(configuration, timeProvider, priceProcessor);
    }
}
