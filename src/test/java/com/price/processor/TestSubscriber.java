package com.price.processor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

class TestSubscriber implements PriceProcessor {

    private final AtomicLong updatesCount = new AtomicLong();
    private final Map<String, Double> prices = new ConcurrentHashMap<>();

    @Override
    public void onPrice(String ccyPair, double rate) {
        updatesCount.getAndIncrement();
        prices.put(ccyPair, rate);
    }

    public double lastKnownPrice(String ccyPair) {
        Double price = prices.get(ccyPair);
        return price == null ? -1D : price;
    }

    public long totalPriceUpdates() { return updatesCount.get(); }

    @Override
    public void subscribe(PriceProcessor priceProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void unsubscribe(PriceProcessor priceProcessor) {
        throw new UnsupportedOperationException();
    }
}
