package com.price.processor;

import net.openhft.chronicle.core.time.SetTimeProvider;
import net.openhft.chronicle.core.time.TimeProvider;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class TestFactory {

    static final int THROTTLING_RATE_IN_MILLIS = 100;

    Configuration testConfiguration(int availableProcessors) {
        return Configuration.builder()
                .maxSubscribers(200)
                .maxCcyPairs(200)
                .subscriberNotificationRateInMillis(THROTTLING_RATE_IN_MILLIS)
                .maxNativeThreadsForSlowMode(availableProcessors)
                .fastModeThresholdInMillis(1000) // fast processing is within 1sec
                .build();
    }

    PriceThrottler priceThrottler(int availableProcessors, TimeProvider timeProvider) {
        return new PriceThrottler(testConfiguration(availableProcessors), timeProvider);
    }

    List<SlowSubscriber> createSlowSubscribers(int count, SlowSubscriber.CpuUsageMode mode, TimeProvider timeProvider) {
        return createSlowSubscribers(count, mode, 30, TimeUnit.MINUTES, timeProvider);
    }

    List<SlowSubscriber> createSlowSubscribers(int count, SlowSubscriber.CpuUsageMode cpuUsageMode, long elapsedTime, TimeUnit timeUnit, TimeProvider timeProvider) {
        return IntStream.range(0, count).mapToObj(i -> new SlowSubscriber(elapsedTime, timeUnit, cpuUsageMode, timeProvider)).collect(Collectors.toList());
    }

    List<FastSubscriber> createFastSubscribers(int count, SetTimeProvider timeProvider) {
        return IntStream.range(0, count).mapToObj(i -> new FastSubscriber(timeProvider)).collect(Collectors.toList());
    }
}
