package com.price.processor;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.time.TimeProvider;
import java.util.concurrent.TimeUnit;

@Setter
@Slf4j
class SlowSubscriber extends TestSubscriber {

    enum CpuUsageMode { WAIT, BUSY_WAIT, MIXED }

    private volatile long delay;
    private volatile TimeUnit delayTimeUnit;
    private volatile CpuUsageMode cpuUsageMode;
    private volatile TimeProvider timeProvider;

    public SlowSubscriber(long delay, TimeUnit delayTimeUnit, CpuUsageMode mode, TimeProvider timeProvider) {
        this.delay = delay;
        this.delayTimeUnit = delayTimeUnit;
        this.cpuUsageMode = mode;
        this.timeProvider = timeProvider;
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        super.onPrice(ccyPair, rate);
        switch (cpuUsageMode) {
            case WAIT -> sleep(delay);
            case BUSY_WAIT -> busyWait(delay);
            case MIXED -> mixedWait(delay);
        }
    }

    private void mixedWait(long delay) {
        final long before = timeProvider.currentTime(delayTimeUnit);
        sleep(before, delay / 4);
        busyWait(before, delay / 4);
        sleep(before,delay / 4);
        busyWait(before, delay / 4);
    }

    private void sleep(long delay) {
        sleep(timeProvider.currentTime(delayTimeUnit), delay);
    }

    private void sleep(long before, long delay) {
        try {
            long now = timeProvider.currentTime(delayTimeUnit);
            while (now - before < delay) {
                Thread.sleep(100);
                now = timeProvider.currentTime(delayTimeUnit);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void busyWait(long delay) {
        busyWait(timeProvider.currentTime(delayTimeUnit), delay);
    }

    private void busyWait(long before, long delay) {
        while (timeProvider.currentTime(delayTimeUnit) - before < delay);
    }
}
