package com.price.processor;

import lombok.extern.slf4j.Slf4j;
import net.openhft.chronicle.core.time.SetTimeProvider;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

@Slf4j
public class PriceThrottlerTest {

    private final TestFactory testFactory = new TestFactory();
    private final SetTimeProvider timeProvider = new SetTimeProvider();
    private PriceThrottler priceThrottler;
    private PriceUpdatesEmitter priceUpdatesEmitter;

    private static final int availableProcessors = Runtime.getRuntime().availableProcessors();

    @BeforeEach
    void setUp() {
        priceThrottler = testFactory.priceThrottler(availableProcessors, timeProvider);
        priceUpdatesEmitter = new PriceUpdatesEmitter(priceThrottler);
    }

    @AfterEach
    void tearDown() {
        priceThrottler.closeImmediately();
    }

    @Test
    @DisplayName("Slow subscribers should not impact fast subscribers")
    void fastSubscribersPriorityTest() throws InterruptedException {
        // given: we have 2 fast and 3xCPUs slow (30 min delay) subscribers
        final var ccyPair = "USDRUB";
        final var fastSubscribers = testFactory.createFastSubscribers(2, timeProvider);
        priceThrottler.subscribeAll(fastSubscribers);
        priceThrottler.subscribeAll(testFactory.createSlowSubscribers(availableProcessors, SlowSubscriber.CpuUsageMode.WAIT, timeProvider));

        // when: we emit 10 times price updates
        var lastEmittedPrice = priceUpdatesEmitter.emitMultipleTimes(ccyPair, 10);
        waitTillPriceThrottlerProcessUpdates(); // slow subscribers should enter to slow cycles here

        // then: fast subscribers should be notified by last emitted price
        for (var fastSubscriber : fastSubscribers) {
            Assertions.assertEquals(lastEmittedPrice, fastSubscriber.lastKnownPrice(ccyPair));
        }

        // and: re-check once again after 5 seconds passed, and new slow subscribers connected
        timeProvider.advanceMillis(TimeUnit.SECONDS.toMillis(5));
        priceThrottler.subscribeAll(testFactory.createSlowSubscribers(availableProcessors, SlowSubscriber.CpuUsageMode.BUSY_WAIT, timeProvider));
        priceThrottler.subscribeAll(testFactory.createSlowSubscribers(availableProcessors, SlowSubscriber.CpuUsageMode.MIXED, timeProvider));
        lastEmittedPrice = priceUpdatesEmitter.emitMultipleTimes(ccyPair, 10);
        waitTillPriceThrottlerProcessUpdates();

        // then: fast subscribers should be notified by last emitted price
        for (var fastSubscriber : fastSubscribers) {
            Assertions.assertEquals(lastEmittedPrice, fastSubscriber.lastKnownPrice(ccyPair));
        }
    }

    @Test
    @DisplayName("Throttler should skip updates if some pair ticks too fast, only last price matters")
    void throttlingTest() throws InterruptedException {
        // given: fast subscriber and high ticking pair
        final var ccyPair = "USDEUR";
        final var fastSubscriber= testFactory.createFastSubscribers(1, timeProvider).get(0);
        priceThrottler.subscribe(fastSubscriber);

        // when: we emit 1000 times price updates
        final var lastEmittedPrice = priceUpdatesEmitter.emitMultipleTimes(ccyPair, 1000);
        waitTillPriceThrottlerProcessUpdates();

        // then: fast subscriber should have received only few updates
        Assertions.assertTrue(fastSubscriber.totalPriceUpdates() < 5);
        // and: only last price matters
        Assertions.assertEquals(lastEmittedPrice, fastSubscriber.lastKnownPrice(ccyPair));
    }

    @Test
    @DisplayName("Subscribers should not miss rarely changing prices")
    void rarelyChangingPricesTest() throws InterruptedException {
        // given: slow subscriber, fast subscriber and rarely changing pair
        final var ccyPair = "USDAUD";
        final var slowSubscriber= testFactory.createSlowSubscribers(1, SlowSubscriber.CpuUsageMode.MIXED, timeProvider).get(0);
        final var fastSubscriber = testFactory.createFastSubscribers(1, timeProvider).get(0);

        priceThrottler.subscribe(slowSubscriber);
        priceThrottler.subscribe(fastSubscriber);

        // when: we emit 1 price update per day, then per 12 hours
        priceUpdatesEmitter.emitNow(ccyPair, 10);
        waitTillPriceThrottlerProcessUpdates();
        timeProvider.advanceMillis(TimeUnit.DAYS.toMillis(1)); // 1 day elapsed
        priceUpdatesEmitter.emitNow(ccyPair, 15);
        timeProvider.advanceMillis(TimeUnit.DAYS.toMillis(1)); // 1 day elapsed
        waitTillPriceThrottlerProcessUpdates();
        priceUpdatesEmitter.emitNow(ccyPair, 5);
        timeProvider.advanceMillis(TimeUnit.HOURS.toMillis(12)); // 12 hours elapsed
        waitTillPriceThrottlerProcessUpdates();
        priceUpdatesEmitter.emitNow(ccyPair, 25);
        timeProvider.advanceMillis(TimeUnit.HOURS.toMillis(12)); // 12 hours elapsed
        waitTillPriceThrottlerProcessUpdates();

        // then: both subscribers should be notified about all price updates
        Assertions.assertEquals(4, fastSubscriber.totalPriceUpdates());
        Assertions.assertEquals( 4, slowSubscriber.totalPriceUpdates());
    }

    @Test
    @DisplayName("When a slow subscriber became fast it should receive price update")
    void subscriberDynamicProcessingRateTest() throws InterruptedException {
        // given: slow consumers that make all cores busy, one of them can change its delay
        final var ccyPair = "USDRUB";
        final var slowSubscribers = testFactory.createSlowSubscribers(availableProcessors, SlowSubscriber.CpuUsageMode.BUSY_WAIT, timeProvider);
        final var dynamicSubscriber = slowSubscribers.get(0);
        dynamicSubscriber.setDelay(1); // 1 minute
        priceThrottler.subscribeAll(slowSubscribers);

        // when: we emit first price update
        priceUpdatesEmitter.emitNow(ccyPair, 73);
        waitTillPriceThrottlerProcessUpdates();
        // here all available threads should be busy

        // and: slow subscriber became fast
        timeProvider.advanceMillis(TimeUnit.MINUTES.toMillis(1)); // after 1 minute
        dynamicSubscriber.setDelay(100);
        dynamicSubscriber.setDelayTimeUnit(TimeUnit.MILLISECONDS);
        priceUpdatesEmitter.emitNow(ccyPair, 75);
        waitTillPriceThrottlerProcessUpdates();
        timeProvider.advanceMillis(100);

        // here only one core is available

        // where: one more slow subscriber takes last available core
        priceThrottler.subscribe(testFactory.createSlowSubscribers(1, SlowSubscriber.CpuUsageMode.BUSY_WAIT, timeProvider).get(0));
        // and: new price updates emitted
        priceUpdatesEmitter.emitNow(ccyPair, 80);
        waitTillPriceThrottlerProcessUpdates();
        timeProvider.advanceMillis(100);
        priceUpdatesEmitter.emitNow(ccyPair, 85);
        waitTillPriceThrottlerProcessUpdates();

        // then: fast subscriber should get last update
        Assertions.assertEquals( 85d, dynamicSubscriber.lastKnownPrice(ccyPair));
    }

    // PriceThrottler internally uses scheduler with fixed rate updates. So we need to call sleep to wait till the scheduler wakes up
    private void waitTillPriceThrottlerProcessUpdates() throws InterruptedException {
        Thread.sleep(TestFactory.THROTTLING_RATE_IN_MILLIS * 2);
    }
}
