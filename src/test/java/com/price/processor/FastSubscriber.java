package com.price.processor;

import net.openhft.chronicle.core.time.SetTimeProvider;

public class FastSubscriber extends TestSubscriber {

    private final SetTimeProvider timeProvider;

    public FastSubscriber(SetTimeProvider timeProvider) {
        this.timeProvider = timeProvider;
    }

    @Override
    public void onPrice(String ccyPair, double rate) {
        super.onPrice(ccyPair, rate);
        timeProvider.advanceNanos(1);
    }
}
