package com.price.processor;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;

@Slf4j
public class PriceUpdatesEmitter {

    private final PriceThrottler priceThrottler;
    private final Random random = new Random();

    public PriceUpdatesEmitter(PriceThrottler priceThrottler) {
        this.priceThrottler = priceThrottler;
    }

    /**
     * @return last emitted price
     */
    double emitMultipleTimes(String ccyPair, int times) {
        double price = 0.0;
        while (times > 0) {
            price = emitNow(ccyPair, random.nextDouble() * 100);
            times--;
        }
        return price;
    }

    double emitNow(String ccyPair, double price) {
        priceThrottler.onPrice(ccyPair, price);
        return price;
    }
}
