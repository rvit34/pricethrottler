
**Description**

Here is my implementation of PriceThrottler task. See [PriceProcesor.java](/src/main/java/com/price/processor/PriceProcessor.java) for more task details.

**Prerequisites**

[One](/src/main/java/com/price/processor/impl/lastprice/LastPriceProcessingUnit.java) of my implementation uses virtual threads feature from [Project Loom](https://wiki.openjdk.java.net/display/loom/Main) (not released yet). So you need special early-access [JDK-17-loom build](http://jdk.java.net/loom/)

**Build**

1. Download appropriate jdk from [early-access builds](http://jdk.java.net/loom/) and unpack it to your desired directory;
2. Set JAVA_HOME variable in [setenv.sh](setenv.sh) to the directory with unpacked jdk above;
3. Run [build.sh](build.sh).

**Launch**

To launch application just call [run.sh](run.sh)
To launch tests just call [run_tests.sh](run_tests.sh)

