#!/usr/bin/env bash

export JAVA_HOME=/usr/lib/jvm/jdk-17-loom
export PATH=$JAVA_HOME/bin:$PATH

java --version